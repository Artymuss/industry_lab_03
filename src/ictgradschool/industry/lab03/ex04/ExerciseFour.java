package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        String sentence = getNumberFromUser();

        int decimalPlace = getDecimalPlaceFromUser();

        int dpPosition = getDPPosition(sentence);
        //System.out.println(dpPosition);

        String truncate = dpTruncation(sentence, dpPosition, decimalPlace );
        //System.out.println(truncate);
        printTruncatedAmount(truncate, decimalPlace);
        // TODO Use other methods you create to implement this program's functionality.
    }

    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard
    private String getNumberFromUser () {

        System.out.println("Please enter an amount: ");
        return Keyboard.readInput();
    }

    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard
    private int getDecimalPlaceFromUser(){

        System.out.println("Please enter the number of decimal places: ");
        return  Integer.parseInt(Keyboard.readInput());
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's
    private int getDPPosition(String sentence){

        return  sentence.indexOf(".");

    }
    private String dpTruncation(String sentence, int position, int userDP){

        String truncate = sentence.substring(0,position + 1 + userDP);
        return  truncate;
    }

    // TODO Write a method which prints the truncated amount
    private void printTruncatedAmount (String truncate, int decimalPlace ){
        System.out.println("Amount truncated to " + decimalPlace  + " is: " + truncate );
    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
