package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    private int lowerBound;
    private int upperBound;
    /**
     * TODO Your code here. You may also write additional methods if you like.
     *
     */



    private void start() {
        System.out.print("Lower bound? ");
        lowerBound = Integer.parseInt(Keyboard.readInput());
        System.out.print("Upper bound? ");
        upperBound = Integer.parseInt(Keyboard.readInput());

    }

        private void randomnumber(){
        int rand1 = (int) (lowerBound + (upperBound - lowerBound +1) *Math.random());
        int rand2 = (int) (lowerBound + (upperBound - lowerBound +1) *Math.random());
        int rand3 = (int) (lowerBound + (upperBound - lowerBound +1) *Math.random());

        System.out.println("Three randomly generated numbers: " + rand1 + " " + rand2 + " " + rand3 + " " );

        System.out.println ("Smallest number is " + (int) Math.min(Math.min(rand1,rand2),rand3));
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();
        ex.randomnumber();
    }
}
